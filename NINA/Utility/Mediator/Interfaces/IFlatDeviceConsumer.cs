﻿using NINA.Model.MyFlatDevice;

namespace NINA.Utility.Mediator.Interfaces {

    public interface IFlatDeviceConsumer : IDeviceConsumer<FlatDeviceInfo> {
    }
}