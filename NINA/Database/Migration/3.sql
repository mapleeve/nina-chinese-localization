﻿INSERT OR REPLACE INTO `brightstars` (name,ra,dec,magnitude,syncedfrom) VALUES
 ('Deneb Kaitos',10.89738,-17.986606,2.04,'Simbad'),
 ('Kakkab',220.482315,-47.388199,2.28,'Simbad'),
 ('Ankaa',6.57105,-42.305987,2.4,'Simbad'),
 ('Alpha Reticuli',63.60618,-62.473859,3.33,'Simbad'),
 ('Alpha Arae',262.96038,-49.876145,2.84,'Simbad'),
 ('Alherem',161.69241,-49.420257,2.72,'Simbad'),
 ('Alphaulka',340.66809,-45.113361,2.11,'Simbad');
 
  PRAGMA user_version = 3;